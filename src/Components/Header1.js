import React from 'react';

import {NavLink} from 'react-router-dom'


const Header1 = () => {
    
  return (
    <div>
      <header className="sticky-header">
        <NavLink to='/' className='link'>  <span className='brandlogo'>STAYBUDDY</span></NavLink>
        <nav className='navigation'>
        <NavLink to='/Login' className='link'>Login</NavLink>
        <NavLink to='/Register' className='link'>Register</NavLink>
        
      
        </nav>
      </header>
    </div>
  )
}

export default Header1
