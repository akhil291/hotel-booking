import React from 'react';
import { useDispatch } from 'react-redux';

import {NavLink, useNavigate} from 'react-router-dom'


const Header = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const handleLogout = () => {
    dispatch({type: "LOGOUT", value: ""})
    return navigate('./')
  }

  return (
    <div>
      <header className="header">
        <NavLink to='/Home' className='link'>  <span className='brandlogo'>STAYBUDDY</span></NavLink>
        <nav className='navigation'>
        <NavLink to='/Home' className='link'>Home</NavLink>
        <NavLink to='/contact' className='link'>Contact</NavLink>
        <NavLink to='/AboutUs' className='link'>AboutUs</NavLink>
        <NavLink to='/Bookings' className='link'>My Bookings</NavLink>
        
        <button className='logout-btn'  onClick={handleLogout}>LOGOUT</button>
      
        </nav>
      </header>
    </div>
  )
}

export default Header
