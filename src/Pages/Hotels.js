import React, { useEffect, useState } from "react";
import { Card, CardBody, CardTitle ,CardImg,CardText, Button} from "reactstrap";
import "../App.css";
import { useNavigate } from 'react-router-dom';

import axios from "axios";


function Hotels() {
  const [hotels, sethotels] = useState([]);
  const navigate = useNavigate(); 
  const handleBookNow = (id) => {
    localStorage.setItem("id", id)
    navigate("/HotelDetails");
  };

  useEffect(() => {
    try {
      const fetchData = async () => {
        const response = await axios.get("http://localhost:8008/fetch");
        sethotels(response.data);
      };
      fetchData();
    }catch (err) {
      console.log(err);
     
    };
    
  }, []);



  
  return (
    
    <div>
    {hotels.length > 0 ? (
      <div>
      <div className="card-container">
        {hotels.slice(0, 3).map((hotel, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{hotel.Roomtype}</CardTitle>
              {hotel.Image && (
                <CardImg top width="100%" height="250px" src={hotel.Image} alt={hotel.Roomtype} />
              )}
              <CardText>Price: {hotel.Price}</CardText>
              <CardText> {hotel.Description}</CardText>  
              <CardText> Rating: {hotel.Rating.Rate}({hotel.Rating.Count})</CardText>  
              <Button color="primary" onClick={() => handleBookNow(hotel.ID)}>Book Now</Button>           
            </CardBody>
          </Card>
        ))}
      </div>
    
      <div className="card-container">
        {hotels.slice(3, 6).map((hotel, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{hotel.Roomtype}</CardTitle>
              {hotel.Image && (
                <CardImg top width="100%" height="250px" src={hotel.Image} alt={hotel.Roomtype} />
              )}
              <CardText>Price: {hotel.Price}</CardText>
              <CardText> {hotel.Description}</CardText>  
              <CardText> Rating: {hotel.Rating.Rate}({hotel.Rating.Count})</CardText>  
              <Button color="primary" onClick={() => handleBookNow(hotel.ID)}>Book Now</Button>           
            </CardBody>
          </Card>
        ))}
      </div>
    </div>
      
    ) : (
      <p>No hotels found</p>
    )}
  </div>
  );
}
export default Hotels;
