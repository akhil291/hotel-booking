import axios from "axios";
import React, { useState } from "react";

const url = "http://127.0.0.1:8008/Register";

const SignupForm = () => {
  const [Gender, setGender] = useState("");
  const [Email, setEmail] = useState("");
  const [Password, setPassword] = useState("");
  const [PhoneNumber, setPhoneNumber] = useState("");
  const [UserName, setUserName] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const data = {
        Gender,
        Email,
        Password,
        PhoneNumber,
        UserName,
      };
      const response = await axios.post(url, data);
      console.log(response.data);
      clearFields();
    } catch (err) {
      console.log(err);
    }
  };

  const clearFields = () => {
    setGender("");
    setEmail("");
    setPassword("");
    setPhoneNumber("");
    setUserName("");
  };

  return (
    <div className="signup-container">
      <h2>Register</h2>
      <form onSubmit={handleSubmit} className="signup-form">
        <table><tbody>
        <tr className="form-group">
         <td> <label htmlFor="UserName">UserName:</label></td> 
         <td> <input
            type="text"
            id="UserName"
            name="UserName"
            value={UserName}
            onChange={(event) => {
              setUserName(event.target.value);
            }}
          /></td> 
        </tr>
        <tr className="form-group">
         <td> <label htmlFor="Password">Password:</label></td> 
         <td> <input
            type="password"
            id="password"
            name="password"
            value={Password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          /></td> 
        </tr>
        <tr className="form-group">
          <td><label htmlFor="Gender">Gender:</label></td> 
         <td> <input
            type="text"
            id="gender"
            name="gender"
            value={Gender}
            onChange={(event) => {
              setGender(event.target.value);
            }}
          /></td> 
        </tr>
        <tr className="form-group">
         <td> <label htmlFor="Email">Email:</label></td> 
          <td><input
            type="text"
            id="email"
            name="email"
            value={Email}
            onChange={(event) => {
              setEmail(event.target.value);
            }}
          /></td> 
        </tr>
        <tr className="form-group">
         <td><label htmlFor="PhoneNumber">PhoneNumber:</label></td> 
         <td> <input
            type="text"
            id="PhoneNumber"
            name="PhoneNumber"
            value={PhoneNumber}
            onChange={(event) => {
              setPhoneNumber(event.target.value);
            }}
          /></td> 
        </tr>
        <tr className="form-group">
          <td colSpan={2}> <button type="submit" className="submit-button">Register</button></td>
        </tr>
        </tbody></table>
      </form>
    </div>
  );
};

export default SignupForm;
