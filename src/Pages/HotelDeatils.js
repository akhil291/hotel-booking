import React, { useEffect, useState } from "react";
import axios from "axios";
import "../App.css";

import PaymentPage from "./Payment";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
let  daysStayed=0;



function HotelDetails() {
  const id = localStorage.getItem("id");
  const [hotel, sethotel] = useState({});
  const [checkInDate, setCheckInDate] = useState("");
  const [checkOutDate, setCheckOutDate] = useState("");
  const [numMembers, setNumMembers] = useState(1);
  const [totalPrice, setTotalPrice] = useState(0);
  const [showPayment, setShowPayment] = useState(false);
  const email = useSelector((state) => state.email)
  const [roomtype, setRoom] = useState('')
  const [buttonDisabled, setButtonDisabled] = useState(true);
 

  const navigate = useNavigate();
 
  useEffect(() => {
    try {
      const fetchData = async () => {
        const response = await axios.get(
          `http://localhost:8008/gethotelbytype/${id}`
        );
        console.log("Hotel Data:", response.data);
        sethotel(response.data);
      };
      fetchData();
    } catch (err) {
      console.log(err);
    }
  }, [id]);
  useEffect(() => {
    console.log("Check-in Date:", checkInDate);
    console.log("Check-out Date:", checkOutDate);
    console.log("Hotel Price:", parseInt(hotel.Price));
    console.log("Number of Members:", numMembers);

    if (checkInDate && checkOutDate && parseInt(hotel.Price)) {
      const startDate = new Date(checkInDate);
      const endDate = new Date(checkOutDate);
   
      if (startDate < endDate) {
         daysStayed = Math.ceil(
          (endDate - startDate) / (1000 * 60 * 60 * 24)
        );
        console.log("Days Stayed:", daysStayed);
         
        console.log(hotel.Price)
        const Price = daysStayed * parseInt(hotel.Price);

        console.log("Total Price:", totalPrice);
        setTotalPrice(Price);
        setRoom(hotel.Roomtype)
        setButtonDisabled(false);
      } else {
        setTotalPrice(0);
        setButtonDisabled(true);
      }
    } else {
      setTotalPrice(0);
      setButtonDisabled(true);
    }
  }, [checkInDate, checkOutDate, hotel.Price, numMembers,totalPrice,hotel.Roomtype]);

  const handleCheckInDateChange = (e) => {
    const selectedDate = new Date(e.target.value);
    setCheckInDate(e.target.value);
    setCheckOutDate("");
    const nextDay = new Date(selectedDate);
    nextDay.setDate(selectedDate.getDate() + 1);
    const nextDayString = nextDay.toISOString().split("T")[0];
    document.getElementById("checkOutDate").setAttribute("min", nextDayString);
  };
  
  const handlePaymentSuccess = () => {
    setShowPayment(false);
    alert("Payment successful! Your room is booked.");
  
    navigate("/Home");
  };

  const handleBookNow = () => {
    try{
      const booking = {
        email,
        roomtype,
        checkInDate,
        checkOutDate

      }
      console.log(roomtype);
      const res = axios.post("http://localhost:8008/addBooking", booking)
      console.log(res.data);
    }catch(err){

    }
    setShowPayment(true);
  };

  return (
    <div className="hotel-details-container">
    <div className="hoteldetails1">
      {showPayment ? (
        <PaymentPage
          totalPrice={totalPrice}
          onPaymentSuccess={handlePaymentSuccess}
          daysStayed={daysStayed}
        />
      ) : (
        <>
          <div className="date1">
            <div className="Checkin">
              <label>Check-in Date: </label>
              <input
                type="date"
                value={checkInDate}
                min={new Date().toISOString().split("T")[0]}
                onChange={handleCheckInDateChange}
              />
            </div>
            <div className="Checkout">
              <label>Check-out Date: </label>
              <input
                type="date"
                id="checkOutDate"
                value={checkOutDate}
                min={checkInDate}
                onChange={(e) => setCheckOutDate(e.target.value)}
              />
            </div>
          </div>
          <div className="num-members-dropdown">
            <label>Number of Members:</label>
            <select
              value={numMembers}
              onChange={(e) => setNumMembers(parseInt(e.target.value))}
            >
              <option value={1}>1</option>
              <option value={2}>2</option>
              <option value={3}>3</option>
            </select>
          </div ><div className="room-details">
          <p>{hotel.Roomtype}</p>
          <p> {hotel.Description}</p>
          <p>Price: {parseInt(hotel.Price)}</p>
          <p>Total Price: {totalPrice}</p>
          <img src={hotel.Image} alt="img1" /> <br></br>
          <button onClick={handleBookNow} className="book-now-button"  disabled={buttonDisabled}>
            BOOK NOW
          </button></div> 
        </>
      )}
    </div>
  </div>
  
  );
}

export default HotelDetails;
