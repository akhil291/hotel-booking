import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

function LoginForm() {
    const navigate = useNavigate();
    const [Email, setEmail] = useState('');
    const [Password, setPassword] = useState('');
    const dispatch = useDispatch();

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            if (!Email || !Password) {
                alert("Please enter both email and password.");
                return;
            }

            // Perform login request
            await axios.post('http://localhost:8008/Login', { Email, Password });
            
            // Dispatch login action
            dispatch({type: "LOGIN", value: Email});
            
            // Navigate the user based on their login status
            if (Email === 'admin@example.com') {
                return navigate('/Admin');
            } else {
                return navigate('/Home');
            }
        } catch (error) {
            console.error('Login failed', error);
            if (error.response && error.response.status === 401) {
                alert("Incorrect email or password. Please enter correct details.");
            } else {
                alert("An error occurred. Please try again later.");
            }
        }
    };

    return (
        <div id='bodylogin'>
            <div className="login-container">
                <h2>Login</h2>
                <form onSubmit={handleSubmit} className="login-form">
                    <input type="email" placeholder="Email" value={Email} onChange={(e) => setEmail(e.target.value)} />
                    <input type="password" placeholder="Password" value={Password} onChange={(e) => setPassword(e.target.value)} />
                    <div style={{ textAlign: 'center' }}>
                        <button type="submit" className="submit-button">Login</button>
                    </div>
                    <br></br>
                    <div className='login-forgot'>
                        <a href='Forgot'>Forgot Password?</a>
                        <a href='register'>Register</a>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default LoginForm;
