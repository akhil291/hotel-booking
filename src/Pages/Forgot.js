import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const ForgotPwd = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    const navigate = useNavigate();

    const handlePassword = async (email) => {
        try {
            await axios.put(`http://localhost:8008/updateUser/${email}`, { "Password": password });
            alert("Password Updated Successfully");
            navigate('/Login');
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className="login" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
            <div className="login-form" style={{ border: '1px solid #ccc', padding: '20px', borderRadius: '5px', boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)' }}>
                <table>
                    <tr>
                        <td><label htmlFor="email">Email:</label></td>
                        <td><input id="email" type="email" placeholder="Email" value={email} required onChange={(e) => setEmail(e.target.value)} /></td>
                    </tr>
                    <tr>
                        <td><label htmlFor="password">Enter New Password:</label></td>
                        <td>
                            <input id="password" type={showPassword ? "text" : "password"} placeholder="Password" value={password} required onChange={(e) => setPassword(e.target.value)} />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <label htmlFor="check">Show Password</label>
                            <input
                                id="check"
                                type="checkbox"
                                value={showPassword}
                                onChange={() => setShowPassword(prev => !prev)}
                            />
                        </td>
                    </tr>
              <tr>
               <td colSpan='2'> <button type="submit" id="login-btn" onClick={() => handlePassword(email)}>Change Password</button></td>
                </tr>
                </table>
            </div>
        </div>
    );
};

export default ForgotPwd;
