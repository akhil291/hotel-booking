
import img1 from '../Images/myhome.jpg'
import '../App.css';

import { useNavigate } from 'react-router-dom';

function Home() {
    const navigate = useNavigate()
    const handlesubmit =async(e) => {
         return navigate('/Hotels')
    }
  return (
    
    <div className='Home' style={{ position: 'relative' }}>
     <img src={img1} alt='...' />
     <div className='Home1' style={{ position: 'absolute', top: '25%', left: '50%', transform: 'translate(-50%, -50%)', textAlign: 'center' }}>
        <h2>Welcome to StayBuddy</h2>
        <button onClick={handlesubmit} className='Booknow'>BOOK NOW</button>
        
      </div>
    </div>
  )
}
export default Home
