import React from 'react'
import img1 from '../Images/IMG_9397.jpg'
import img2 from '../Images/IMG_9399.jpg'
import img3 from '../Images/yaswanth.jpg'

function Aboutus() {
  return (
    <div className="about-us">
    <h1>About Us</h1>

    <p>Founded in 1996 in Amsterdam, STAYBUDDY has grown from a small Dutch startup to one of the world’s leading digital hotels companies. Part of Booking Holdings Inc. (NASDAQ: BKNG), STAYBUDDY mission is to make it easier for everyone to experience the world.

By investing in the technology that helps take the friction out of travel, Booking seamlessly connects millions of travellers with memorable experiences, a range of transport options and incredible places to stay - from homes to hotels and much more. As one of the world’s largest travel marketplaces for both established brands and entrepreneurs of all sizes.</p>
    
    <h1>Our Team</h1>
    <div class="main">
        
      <div class="person">
        <img src={img2} alt="FounderImage"/>
        <div class="person-info">
            <h1>AKHILANANDA</h1>
          <h2>Founder</h2>
          
        </div>
      </div>
      <div class="person">
        <img src={img3} alt="Co-FounderImage"/>
        <div class="person-info">
            <h1>YASWANTH</h1>
          <h2>Co-Founder</h2>
        </div>
      </div>
      <div class="person">
        <img src={img1}alt="ManagerImage"/>
        <div class="person-info">
            <h1>DEERAJ</h1>
          <h2>CEO</h2>
        </div>
      </div>
    </div>
  </div>
  
  )
}

export default Aboutus

