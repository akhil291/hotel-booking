import React from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
function Contact() {
    const navigate = useNavigate();
    const handleChange = ()=>{
        navigate('/Home')
    }
    
  return (
    <div>
        <Outlet/>
     <div class="container">
        
      <div class="contact-box">
        <h2>Contact Us</h2>
        <form action="#">
          <input type="text" name="name" placeholder="Your Name" required />
          <input type="email" name="email" placeholder="Your Email" required />
          <textarea
            name="message"
            placeholder="Your Message"
            rows="5"
            required
          ></textarea>
          <button type="submit">Send Message</button>
          
        </form>
        
      </div>
      <button onClick={handleChange}>Back To Home</button>
    
    </div>
    </div>

    
  )
}

export default Contact

