import React, { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import "../App.css";

function Bookings() {
  const [bookings, setBookings] = useState([]);
  const email = useSelector((state) => state.email);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`http://localhost:8008/getBookings/${email}`);
        setBookings(response.data);
      } catch (error) {
        console.error("Error fetching bookings:", error);
      }
    };

    fetchData();
  }, [email]);

  return (
    <div>
      {bookings.length > 0 ? (
        <div>
          <table className="bookings-table">
            <thead>
              <tr>
                <th>Room Type</th>
                <th>Check-in Date</th>
                <th>Check-out Date</th>
              </tr>
            </thead>
            <tbody>
              {bookings.map((booking, index) => (
                <tr key={index}>
                  <td>{booking.Roomtype}</td>
                  <td>{booking.CheckinDate}</td>
                  <td>{booking.CheckoutDate}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ) : (
        <p>No bookings found</p>
      )}
    </div>
  );
}

export default Bookings;
