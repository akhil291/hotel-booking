import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";

function PaymentPage({ totalPrice, onPaymentSuccess, daysStayed }) {
  const id = localStorage.getItem("id");
  const [cardNumber, setCardNumber] = useState("");
  const [expiryDate, setExpiryDate] = useState("");
  const [cvv, setCvv] = useState("");
  const [hotel, setHotel] = useState({});
  const [user, setUser] = useState({});
  const email = useSelector((state) => state.email);

  useEffect(() => {
    try {
      const fetchData = async () => {
        const response = await axios.get(
          `http://localhost:8008/gethotelbytype/${id}`
        );
        console.log("Hotel Data:", response.data);
        setHotel(response.data);
      };
      fetchData();
      const fetchData1 = async () => {
        const response = await axios.get(
          `http://localhost:8008/getuserbyemail/${email}`
        );
        console.log(email);
       
        setUser(response.data);
      };
      fetchData1();
    } catch (err) {
      console.log(err);
    }
  }, [email, id]);

  const formatCardNumber = (input) => {
    
    return input.replace(/\W/g, '').replace(/(.{4})/g, '$1 ').trim();
  };

  const formatExpiryDate = (input) => {
  
    return input
      .replace(/\W/g, '')
      .replace(/^(.{2})/, '$1/')  
      .trim();
  };

  const handleCardNumberChange = (e) => {
    let input = e.target.value;
   
    input = input.replace(/\D/g, '');
   
    input = input.slice(0, 16);
    const formattedInput = formatCardNumber(input);
    setCardNumber(formattedInput);
  };

  const handleExpiryDateChange = (e) => {
    const formattedInput = formatExpiryDate(e.target.value);
    setExpiryDate(formattedInput);
  };

  const handlePayment = () => {
    if (cardNumber && expiryDate && cvv) {
      onPaymentSuccess();
    } else {
      alert("Please enter card details to proceed with payment.");
    }
  };

  const handleCvvChange = (e) => {
    let input = e.target.value;
   
    input = input.replace(/\D/g, '');
 
    input = input.slice(0, 3);
    setCvv(input);
  };

  return (
    <div className="payment-page">
      <div className="room-type">{hotel.Roomtype}</div>
      <div className="payment-details">
        <div className="user-details">
          <p>UserName: {user.UserName}</p>
          <p>Email: {user.Email}</p>
        </div>
        <div className="days-total">
          <p>Days Stayed: {daysStayed}</p>
          <p>Total Price: {totalPrice}</p>
        </div>
      </div>
      <h2>Payment Details</h2>
      <div className="card-details">
        <div className="card-number">
          <label>Card Number : </label>
          <input
            type="text"
            value={cardNumber}
            onChange={handleCardNumberChange}
            placeholder="Enter card number"
          />
        </div>
        <div className="expiry-cvv">
          <div>
            
            <input
              type="text"
              value={expiryDate}
              onChange={handleExpiryDateChange}
              placeholder="MM/YY"
            />
          </div>
          <div>
            
            <input
              type="text"
              value={cvv}
              onChange={handleCvvChange}
              placeholder="CVV"
            />
          </div>
        </div>
      </div>
      <button className="pay-now-btn" onClick={handlePayment}>Pay Now</button>
    </div>
  );
}

export default PaymentPage;
