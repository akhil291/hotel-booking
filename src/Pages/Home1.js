
import img12 from '../Images/hotelback.jpeg'
import '../App.css';

import { useNavigate } from 'react-router-dom';

function Home1() {
    const navigate = useNavigate()
    const handlesubmits =async(e) => {
         return navigate('/Login')
    }
  return (
    
    <div className='Home' style={{ position: 'relative' }}>
     <img src={img12} alt='...' />
     <div className='Home1' style={{ position: 'absolute', top: '25%', left: '50%', transform: 'translate(-50%, -50%)', textAlign: 'center' }}>
        <h2>Welcome to StayBuddy</h2>
        <button onClick={handlesubmits} className='Booknow'>Let's Get Started</button>
        
      </div>
    </div>
  )
}
export default Home1
