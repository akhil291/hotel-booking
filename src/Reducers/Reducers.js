const initialState = {
    login: false,
    email: ""
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case "LOGIN":
            return {
                ...state, login: true, email: action.value
            }
        case "LOGOUT":
            return{
                ...state, login: false, email: action.value
            }
        default: 
            return state
    }
}

export default reducer