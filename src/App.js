import { Route, Routes } from "react-router-dom";
import './App.css';
import Home from "./Pages/Home";
import Contact from "./Pages/Contact";
import Aboutus from "./Pages/Aboutus";
// import header from "./Components/Header";
import Hotels from "./Pages/Hotels";
import LoginForm from "./Pages/Login";
import SignupForm from "./Pages/Signup";
import Admin from "./Pages/Admin";
import HotelDetails from "./Pages/HotelDeatils";
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import Header1 from "./Components/Header1";
import { useSelector } from "react-redux";
import ForgotPwd from "./Pages/Forgot";
import Bookings from "./Pages/MyBookings";
import Home1 from "./Pages/Home1";



function App() {
  const login = useSelector((state) => state.login)
  return (
    <div className="App">
      {login ? <Header /> : <Header1/>}
      
     
      <Routes>
      <Route path="/" element={<Home1/>} /> 
        <Route path="/Home" element={<Home/>} />
        <Route path="/contact" element={<Contact />}/>
        <Route path="/AboutUs" element={<Aboutus />}/>
        <Route path = "/Hotels" element = {<Hotels />}/>
        <Route path = "/Login" element = {<LoginForm />}/>
        <Route path = "/Register" element = {<SignupForm />}/>
        <Route path = "/Admin" element = {<Admin />}/>
        <Route path = "/HotelDetails" element = {<HotelDetails />}/>
        <Route path = "/Forgot" element = {<ForgotPwd />}/>
        <Route path = "/Bookings" element = {<Bookings />}/>
        
        </Routes>
        <Footer className="Footer" />
    </div>
  );
}

export default App;
