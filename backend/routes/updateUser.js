const express = require("express");

const router = express.Router();

const mongoClient = require("mongodb").MongoClient;

const bcrypt = require("bcrypt");

module.exports = router.put("/:email", async(req, res) => {
    console.log(req.body.Password);
    
    const hashPassword = await bcrypt.hash(req.body.Password, 12);

  mongoClient.connect("mongodb://localhost:27017/userdetails",  (err, db) => {
    if (err) {
      res.status(500);
      res.send("Internal server error");
    } else {
      let Email = req.params.email  
      db.collection("userdetail").updateOne({'Email':Email},{$set:{'Password':hashPassword}} ,(err, result) => {
        if (err) throw err;
        else {
          res.send("Sucessfully updated User");
        }
      });
    }
  });
});
