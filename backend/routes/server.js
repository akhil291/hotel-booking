const express = require('express');

const router = express.Router();

const mc = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');

router.post('/',(req,res)=>{
    const {Email,Password} = req.body;
    mc.connect('mongodb://localhost:27017/userdetails',(err,db)=>{
        if(err){
            res.status(500).send("Internal Server Error");
            return;
        }
        else{
            db.collection('userdetail').findOne({"Email":Email},(err,user)=>{
                if(err){
                    res.status(500).send("Internal Server Error");
                    
                    return;
                }
                
                if(!user){
                    res.status(404).send("User not found");
                    return;
                }
                bcrypt.compare(Password,user.Password,(err,result)=>{
                    if(err){
                        res.status(500).send("Internal Server Error");
                        return;
                    }
                    if(result){
                        res.send("<h1>Login SuccessFul</h1>")
                    }else{
                        res.status(401).send("<h1>Invalid Password</h1>")
                    }
                })
                // if(Password === user.Password){
                //     res.send("<h1>Login SuccessFul</h1>")
                // }else{
                //     res.status(401).send("<h1>Invalid Password</h1>")
                // }
                
            })
        }
    })
});

module.exports = router;