const express = require("express");

const router = express.Router();

const mongoClient = require("mongodb").MongoClient;

const bcrypt = require("bcrypt");

module.exports = router.post("/", async(req, res) => {
    console.log(req.body.Password);
    
    const hashPassword = await bcrypt.hash(req.body.Password, 12);

  mongoClient.connect("mongodb://localhost:27017/userdetails",  (err, db) => {
    if (err) {
      res.status(500);
      res.send("Internal server error");
    } else {
      const Students = {
        "Email": req.body.Email,
        "Password": hashPassword,
        "UserName" :req.body.UserName,
        "PhoneNumber":parseInt(req.body.PhoneNumber),
        "Gender": req.body.Gender
      };
      db.collection("userdetail").insertOne(Students, (err, result) => {
        if (err) throw err;
        else {
          res.send("Sucessfully Created User");
        }
      });
    }
  });
});
