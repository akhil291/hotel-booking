const express = require("express")

const router = express.Router()

const mongodb = require("mongodb")


const mongoClient = mongodb.MongoClient
 
module.exports = router.get("/:email", (req, res) => {
    mongoClient.connect("mongodb://localhost:27017/userdetails", (err, db) => {
        
        if (err){
            res.status(500)
            res.send("Internal server error")
        }
        
        else{
            console.log("DB connected")

            var email = req.params['email']
            
            db.collection("userdetail").findOne({"Email":email }, (err, result) => {
                if(err){
                    res.send(err)
                }else{
                    res.status(200).json(result)
                }
            })

            
        }
    })
})