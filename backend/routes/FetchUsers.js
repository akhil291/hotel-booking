const express = require('express')

const router = express.Router();

const mongoClient = require('mongodb').MongoClient;

const fetch = router.get('/',(req, res)=>{
    mongoClient.connect('mongodb://localhost:27017/userdetails',(err, db)=>{
        if(err){
            res.status(500).send("Internal Server Error");
            return;
        }
        else{
            console.log("DB connected");
            db.collection('userdetail').find().toArray((err, data)=>{
                if(err) throw err;
                else{
                    if(data.length > 0){
                        res.status(200).json(data);
                    }else{
                        res.status(404).send("user not found");
                    }
                }
            })
        }
    })
})
module.exports = fetch;