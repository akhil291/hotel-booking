const express = require('express')
const cors = require('cors')
const app = express();
app.use(express.json())
app.use(cors())
app.use('/Login',require('./routes/server'))
app.use('/fetch', require('./routes/Fetch'))
app.use('/Register',require('./routes/Register'))
app.use('/Fetchusers',require('./routes/FetchUsers'))
app.use('/gethotelbytype',require('./routes/gethotelbytype'))
app.use('/getuserbyemail',require('./routes/FetchUserByEmail'))
app.use('/updateUser',require('./routes/updateUser'))
app.use('/addBooking',require('./routes/addBooking'))
app.use('/getBookings',require('./routes/GetBookingsByEmail'))
app.listen(8008,()=>{
    console.log("running...");
})